<?php

 $data = file_get_contents('php://input');
 $json = json_decode($data, true);

 echo json_encode([
     'status' => 'OK',
     'message' => $json['number']
 ]);