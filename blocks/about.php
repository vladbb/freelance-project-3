
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>О компании</title>
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
</head>
<body>
    <div class="wrapper">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <a href="/" class="navbar-brand">AvtoInfo</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarContent">
                    <ul class="navbar-nav mr-auto p-2">
                        <li class="nav-item">
                            <a href="/" class="nav-link">Главная</a>
                        </li>
                        <li class="nav-item">
                            <a href="/blocks/about.php" class="nav-link">О компании</a>
                        </li>
                    </ul>
                    <ul class="navbar-nav d-flex">
                        <li class="nav-item">
                            <a href="/blocks/login.php" class="nav-link btn btn-outline-secondary">Авторизация</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <section class="about">
            <div class="container">
                <div class="content p-3 mt-4 mb-4">
                    <h3>О компании</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vitae et leo duis ut diam quam nulla porttitor. Nunc vel risus commodo viverra maecenas accumsan lacus vel. Aliquet bibendum enim facilisis gravida neque convallis a. Facilisis sed odio morbi quis commodo odio aenean. Pretium lectus quam id leo in. Feugiat in fermentum posuere urna nec. Ultrices neque ornare aenean euismod elementum nisi quis eleifend quam. Vitae turpis massa sed elementum tempus egestas sed. Egestas purus viverra accumsan in nisl nisi scelerisque eu ultrices.</p>
                    <p>Eget lorem dolor sed viverra ipsum nunc aliquet bibendum. Curabitur gravida arcu ac tortor dignissim convallis aenean et tortor. Sit amet nisl suscipit adipiscing bibendum est ultricies. Gravida in fermentum et sollicitudin ac orci phasellus egestas. Viverra nam libero justo laoreet sit amet cursus sit. Imperdiet proin fermentum leo vel orci porta non. Ut lectus arcu bibendum at varius. Eu feugiat pretium nibh ipsum consequat nisl vel pretium lectus. Ultrices in iaculis nunc sed augue lacus viverra vitae congue. Pharetra convallis posuere morbi leo urna molestie.</p>
                    <p>Mattis vulputate enim nulla aliquet porttitor lacus. Facilisi nullam vehicula ipsum a. Dolor morbi non arcu risus quis varius quam. At tellus at urna condimentum mattis pellentesque id. Tellus rutrum tellus pellentesque eu tincidunt tortor aliquam nulla. Nam libero justo laoreet sit amet cursus sit amet. Pharetra vel turpis nunc eget. Est pellentesque elit ullamcorper dignissim cras tincidunt. Convallis a cras semper auctor neque. Donec ultrices tincidunt arcu non. Bibendum ut tristique et egestas quis ipsum. Purus ut faucibus pulvinar elementum integer enim. Sit amet risus nullam eget felis. Ridiculus mus mauris vitae ultricies leo integer. Lacus vestibulum sed arcu non odio euismod lacinia at quis. Sapien pellentesque habitant morbi tristique. Eget duis at tellus at. Velit egestas dui id ornare arcu odio ut sem nulla. Laoreet sit amet cursus sit.</p>
                </div>
            </div>
        </section>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js" integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>
</body>
</html>

