<?php
date_default_timezone_set('Europe/Moscow');
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Данные об авто</title>
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
</head>
<body>

<div class="wrapper">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a href="/" class="navbar-brand">AvtoInfo</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarContent">
                <ul class="navbar-nav mr-auto p-2">
                    <li class="nav-item">
                        <a href="/" class="nav-link">Главная</a>
                    </li>
                    <li class="nav-item">
                        <a href="/blocks/about.php" class="nav-link">О компании</a>
                    </li>
                </ul>
                <ul class="navbar-nav d-flex">
                    <li class="nav-item">
                        <a href="/blocks/login.php" class="nav-link btn btn-outline-secondary">Авторизация</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <section class="about">
        <div class="container">
            <div class="content p-3 mt-4">
                <div class="row">
                    <div class="col-md-2 col-sm m-4 image">
                        <img class="image" src="../img/auto-logo.png" alt="лого автомобиля">
                    </div>
                    <div class="col ml-md-4">
                        <p class="data-time text-secondary">Дата проверки: <?=date('d.m.Y H:i')?> (мск)</p>
                        <span class="hiden con">
                            <span>
                                <span class="auto-mark" id="mark"></span>
                                <span class="auto-mark" id="model"></span>
                            </span>
                            <div class="row">
                                <div class="col">
                                    <p class="vin"><b>VIN: </b><span id="vin"></span></p>
                                </div>
                                <div class="col">
                                    <p class="gos"><b>Госномер: </b><span id="gos"></span></p>
                                </div>
                            </div>
                        </span>
                        <span class="spinner">
                            <img class="spinner-bg" src="../img/spinner.gif" alt="">
                        </span>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col">
                        <p>Год производства: <span class="hiden con" id="year"></span>
                            <span class="spinner">
                                <img class="spinner" src="../img/spinner.gif" alt="">
                            </span>
                        </p>
                        <p>Категория ТС:
                            <span class="spinner">
                                <img class="spinner" src="../img/spinner.gif" alt="">
                            </span>
                        </p>
                        <p>Расположение руля:
                            <span class="spinner">
                                <img class="spinner" src="../img/spinner.gif" alt="">
                            </span>
                        </p>
                    </div>
                    <div class="col">
                        <p>Тип двигателя:
                            <span class="spinner">
                                <img class="spinner" src="../img/spinner.gif" alt="">
                            </span>
                        </p>
                        <p>Мощность двигателя:
                            <span class="spinner">
                                <img class="spinner" src="../img/spinner.gif" alt="">
                            </span>
                        </p>
                        <p>Обьем двигателя: <span class="hiden con" id="volume"></span>
                            <span class="spinner">
                                <img class="spinner" src="../img/spinner.gif" alt="">
                            </span>
                        </p>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </section>
</div>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js" integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>
<script src="../js/preloader.js"></script>
</body>
</html>


