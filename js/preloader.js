const data = {
    mark: '',
    model: '',
    year: '',
    gos: '',
    vin: '',
    volume: ''
}

const preloader = document.getElementsByClassName('spinner');
const content = document.getElementsByClassName('con');

const listPromise = fetch('https://jsonplaceholder.typicode.com/users');
listPromise
    .then(data => data.json())
    .then(auto => {
        changeDataClass();
        setData(auto);
        viewData();
    });

function changeDataClass() {
    for (let i = 0; i < preloader.length; i++) {
        preloader[i].classList.add("hiden");
    }
    for (let i = 0; i < content.length; i++) {
        content[i].classList.remove("hiden");
    }
}

function setData(auto) {
    auto.forEach(item => {
        if (item.id === 1) {
            data.mark = item['username'];
            data.model = item['name'];
            data.year = item['address']['suite'];
            data.gos = item['address']['zipcode'];
            data.vin = item['email'];
            data.volume = item['address']['geo']['lat'];
        }
    });
}

function viewData() {
    for (let key in data) {
        document.querySelector('#' + key).innerHTML = data[key];
    }
}

