const user = {
    number: ''
}

$(document).ready(function() {
    $("#gos-num").mask("aa-999-aa");
    $("#vin-num").mask("*****************");

    $('#gos-btn').click(function(e) {
        e.preventDefault();
        user.number = $("#gos-num").val();

        if (user.number) {
            sendData(user);
        }
    })

    $('#vin-btn').click(function(e) {
        e.preventDefault();
        user.number = $("#vin-num").val();

        if(user.number) {
            sendData(user);
        }

    })
});

function sendData(user) {
    postData(user)
        .then((res) => res.json())
        .then((res) => {
            if (res["status"] === "OK") {
                console.log(res["message"]);
                window.location.href = '/blocks/report.php';
            }
            console.log(res);
        });
}

const postData = (body) => {
    return fetch('../server.php', {
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
    });
}