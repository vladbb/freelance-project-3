<?php
require_once 'db.php';
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>AvtoInfo - Главная</title>

    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
    <div class="wrapper">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <a href="/" class="navbar-brand">AvtoInfo</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarContent">
                    <ul class="navbar-nav mr-auto p-2">
                        <li class="nav-item">
                            <a href="/" class="nav-link">Главная</a>
                        </li>
                        <li class="nav-item">
                            <a href="/blocks/about.php" class="nav-link">О компании</a>
                        </li>
                    </ul>
                    <ul class="navbar-nav d-flex">
                        <li class="nav-item">
                            <?php if (isset($_COOKIE['user'])): ?>
                                <a href="/auth/exit.php" class="nav-link btn btn-outline-secondary">Выйти</a>
                            <?php else: ?>
                                <a href="/blocks/login.php" class="nav-link btn btn-outline-secondary">Авторизация</a>
                            <?php endif;?>

                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <section class="main">
            <div class="container">
                <div class="content p-3 mt-4">
                    <ul class="nav nav-pills mb-3 mt-2" id="pills-tab" role="tablist">
                        <li class="nav-item mr-3" role="presentation">
                            <a class="nav-link active btn btn-outline-secondary" id="pills-gos-tab" data-toggle="pill" href="#pills-gos" role="tab" aria-controls="pills-gos" aria-selected="true">Гос. номер</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link btn btn-outline-secondary" id="pills-vin-tab" data-toggle="pill" href="#pills-vin" role="tab" aria-controls="pills-vin" aria-selected="false">VIN-код</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-gos" role="tabpanel" aria-labelledby="pills-gos-tab">
                            <h4>Поиск по госномеру автомобиля</h4>
                            <form action="server.php" method="post" id="form-gos">
                                <div class="form-group">
                                    <div class="background">
                                        <div class="img-back"><img src="img/gosnum.webp" alt="" class="gosnum"></div>
                                        <input type="text" class="form-control" id="gos-num" name="gos_num" placeholder="AB-123-CD">
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-dark" id="gos-btn">Проверить</button>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="pills-vin" role="tabpanel" aria-labelledby="pills-vin-tab">
                            <h4>Поиск по VIN-коду</h4>
                            <form action="server.php" method="post" id="form-vin">
                                <div class="form-group">
                                    <div class="background">
                                        <div class="img-back"><img src="img/gosnum.webp" alt="" class="gosnum"></div>
                                        <input type="text" class="form-control" id="vin-num" name="vin_code" placeholder="">
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-dark" id="vin-btn">Проверить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js" integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>
    <script src="js/jquery.maskedinput.min.js"></script>
    <script src="js/submit_forms.js"></script>
</body>
</html>
